// Copyright (C) 2021 Ondřej Míchal

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

#define _DEFAULT_SOURCE

#include <arpa/inet.h>
#include <err.h>
#include <errno.h>
#include <getopt.h>
#include <libgen.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <netinet/udp.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

static const char *login = "xmicha80";

static const char *program_name = "";

char *nsaddress = NULL;
int nsport;

char *file_server_addr = NULL;
char file_server_ip[32] = "";
int file_server_port;

char *file_path = NULL;
char *file_name = NULL;

static bool verbose = false;

bool get_file(int sock, char *filename) {
    ssize_t bytes_recv;
    bool found_header = false;
    char msg[256] = "", res[BUFSIZ], *res_prot, *res_type;
    FILE *tmpf;

    if (snprintf(msg, 255, "GET %s FSP/1.0\r\nHostname: %s\r\nAgent: %s\r\n\r\n",
                 filename, file_server_addr, login) < 0) {
        warn("failed to assemble message to fileserver");
        return false;
    }

    tmpf = fopen(".tmp", "w");
    if (tmpf == NULL) {
        // TODO: Handle memory
        warn("failed to open tmp file for writing");
        return false;
    }

    if (send(sock, msg, sizeof(msg), 0) == 1) {
        warn("failed to send message to fileserver");
        return false;
    }

    while (1) {
        bytes_recv = recv(sock, res, sizeof(res), 0);
        if (bytes_recv == -1) {
            warn("failed to receive message");
            return false;
        }

        if (bytes_recv == 0) {
            fprintf(stderr, "finished receiving file\n");
            break;
        }

        // Process header of the response
        if (!found_header) {
            if (sscanf(res, "%ms %ms\r\n", &res_prot, &res_type) == EOF) {
                warn("failed to scan response from fileserver");
                return false;
            }

            // TODO: Handle memory

            // Check protocol
            if (strcmp("FSP/1.0", res_prot) != 0) {
                warnx("invalid protocol in response");
                return false;
            }

            // Check response
            if (strcmp("Success", res_type) != 0) {
                if (strcmp("Not Found", res_type) == 0) {
                    warnx("requested file is not on fileserver");
                } else {
                    warnx("unknown response from fileserver");
                }

                return false;
            }

            free(res_prot);
            free(res_type);

            found_header = true;
            continue;
        }

        // It is safe to cast because bytes_recv holds -1 only on error
        if (fwrite(res, sizeof(char), bytes_recv, tmpf) != (size_t) bytes_recv) {
            warn("failed to write to tmp file");
        }
    }
    fclose(tmpf);

    if (rename(".tmp", filename) != 0) {
        warn("failed to finalize downloaded file");
        return false;
    }

    return true;
}

/*
int update_file_list(char **file_list) {
    int num_of_files = 0;
    // Read file -> get number of lines and length of the longest line

    // *file_list = reallocarray(file_list, 2, 120);

    // Read file -> copy filename to the array

    return num_of_files;
}
*/

bool get_files() {
    int sock, num_of_files = 0;
    char **file_list;
    struct sockaddr_in fs_addr;

    fs_addr.sin_family = AF_INET;
    fs_addr.sin_port = htons(file_server_port);
    fs_addr.sin_addr.s_addr = inet_addr(file_server_ip);

    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock == -1) {
        warn("failed to create tcp socket");
        return false;
    }

    if (connect(sock, (struct sockaddr *) &fs_addr, sizeof(fs_addr)) == -1) {
        warn("failed to connect to fileserver");
        return false;
    }

    file_list = calloc(1, sizeof(file_path));
    if (file_list == NULL) {
        warn("failed to prepare file list");
        return false;
    }
    num_of_files++;
    file_list[0] = file_path;

    // Download index file and all files listed in the index
    if (strcmp(file_name, "*") == 0) {
        if (!get_file(sock, "index")) {
            return false;
        }

        // num_of_files = update_file_list(file_list);
        
        // Remove ./index - on both success and error

        if (num_of_files == -1) {
            warn("failed to update file list");
            return false;
        }
    }

    for (int i = 0; i < num_of_files; i++) {
        if (!get_file(sock, file_list[i])) {
            warnx("failed while getting file %s", file_list[i]);
        }
    }

    shutdown(sock, SHUT_RDWR);
    close(sock);

    free(file_list);
    return true;
}

bool resolve_address() {
    int sock, bytes;
    char msg[128] = "WHEREIS ", res[BUFSIZ],
    *res_type = NULL, *res_msg = NULL, *ip = NULL;
    struct sockaddr_in ns_addr;

    ns_addr.sin_family = AF_INET;
    ns_addr.sin_port = htons(nsport);
    ns_addr.sin_addr.s_addr = inet_addr(nsaddress);

    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock == -1) {
        warn("failed to create socket");
        return false;
    }

    if (connect(sock, (struct sockaddr *) &ns_addr, sizeof(ns_addr)) == -1) {
        warn("failed to connect to nameserver");
        return false;
    }

    strncat(msg, file_server_addr, sizeof(file_server_addr) + 1);
    bytes = send(sock, msg, sizeof(msg), 0);
    if (bytes == -1) {
        warn("failed to send message");
        return false;
    }

    bytes = recvfrom(sock, res, sizeof(res), 0, NULL, NULL);
    if (bytes == -1) {
        warn("failed to receive message");
        return false;
    }

    res_type = strtok(res, " ");
    res_msg = strtok(NULL, "");

    if (strcmp(res_type, "ERR") == 0) {
        if (strcmp(res_msg, "Syntax") == 0) {
            warnx("internal error when sending message to nameserver");
            return false;
        }

        if (strcmp(res_msg, "Not Found") == 0) {
            warnx("unknown domain");
            return false;
        }
    }

    ip = strtok(res_msg, ":");
    strcpy(file_server_ip, ip);
    file_server_port = atoi(strtok(NULL, ""));

    shutdown(sock, SHUT_RDWR);
    close(sock);

    return true;
}

void parse_args(int argc, char *argv[]) {
    int option;
    char *nsport_str = NULL, *endptr;
    bool has_address = false, has_url = false;

    while ((option = getopt(argc, argv, "n:f:v")) != -1) {
        switch (option) {
        case 'n':
            nsaddress = strtok(optarg, ":");
            nsport_str = strtok(NULL, "");

            if (nsport_str == NULL) {
                errx(EXIT_FAILURE, "Nameserver address has to contain port");
            }

            errno = 0;
            nsport = strtol(nsport_str, &endptr, 10);
            if (*endptr != '\0' || errno != 0) {
                errx(EXIT_FAILURE, "Provided namespace port is not an integer");
            }
            
            has_address = true;
            break;
        case 'f':
            if (strncmp(optarg, "fsp://", 6) != 0) {
                errx(EXIT_FAILURE, "Unknown protocol. Supported protocol is \"fsp://\"");
            }

            file_server_addr = strtok(&optarg[6], "/");
            file_path = strtok(NULL, "");
            file_name = basename(file_path);

            if (file_path == NULL) {
                errx(EXIT_FAILURE, "Only file server name specified.");
            }

            has_url = true;
            break;
        case 'v':
            verbose = true;
            break;
        default:
            errx(EXIT_FAILURE, "Invalid option");
            break;
        }
    }

    if (!has_address) {
        errx(EXIT_FAILURE, "Missing nameserver. Use option -n");
    }

    if (!has_url) {
        errx(EXIT_FAILURE, "Missing file url. Use option -f");
    }
}

int main(int argc, char *argv[]) {
    program_name = argv[0];

    parse_args(argc, argv);

    if (!resolve_address()) {
        errx(EXIT_FAILURE, "failed to resolve address");
    }

    if (!get_files()) {
        errx(EXIT_FAILURE, "failed to get files");
    }

    return EXIT_SUCCESS;
}
