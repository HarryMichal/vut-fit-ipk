/*
 * ipk-l4-scan - Port scanner
 * Copyright (C) 2021 Ondřej Míchal <xmicha80>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#define _GNU_SOURCE

#include <arpa/inet.h>
#include <err.h>
#include <errno.h>
#include <error.h>
#include <getopt.h>
#include <ifaddrs.h>
#include <netdb.h>
#include <net/if.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <poll.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

/**
 * @brief Range of ports
 * @details set 'end' to -1 if used with a single port
 */
struct port_range {
    int start;
    int end;
};

/**
 * @brief Classic TCP header + 4 bytes for options
 * @details Refer to IPv4 RFC793 Section 3.1
 */
struct ipk_tcphdr {
    struct tcphdr tcphdr;
    uint8_t kind;
    uint8_t len;
    uint16_t mss;
};

/**
 * @brief Pseudo TCP header for IPv4
 * @details Refer to IPv4 RFC793 Section 3.1
 */
struct pseudo_tcphdr_ipv4 {
    uint32_t src_addr;
    uint32_t dst_addr;
    uint8_t zero;
    uint8_t protocol;
    uint16_t tcp_len;
};

/**
 * @brief Pseudo TCP header for IPv6
 * @details Refer to IPv6 RFC2460 Section 8.1
 */
struct pseudo_tcphdr_ipv6 {
    __uint128_t src_addr;
    __uint128_t dst_addr;
    uint32_t ulp_len;
    uint16_t zero1;
    uint8_t zero2;
    uint8_t next_hdr;
};

int app_port = 50542; //<Port number from which packets are sent. Should be incremented after being used once.

char *target_name = NULL; //<Domain/IP of the scanned server
char *interface_name = NULL; //<Name of network interface
int scan_timeout = 5000; //<Waiting time for response from scanned server

struct port_range *tcp_port_pool = NULL; //<Array of tcp port ranges
int tcp_port_poolnd = 1; //<Number of port ranges in an array

struct port_range *udp_port_pool = NULL; //<Array of udp port ranges
int udp_port_poolnd = 1; //<Number of port ranges in an array

// Network interfaces
struct ifaddrs *first_ifaddrs = NULL; //<The first of hosts local interfaces
struct ifaddrs *selected_ifaddr = NULL; //<The interface to bind socket to

static struct option long_options[] = {
    { "interface", optional_argument, 0, 'i' },
    { "pt", required_argument, 0, 't' },
    { "pu", required_argument, 0, 'u' },
    { "wait", required_argument, 0, 'w' },
};

/**
 * @brief tcp_checksum calculates the checksum of a TCP header
 * 
 * @details Inspired by https://github.com/lsanotes/libpcap-tutorial/blob/master/tcpsyndos.c#L247
 * Comments taken from https://www.rfc-editor.org/rfc/rfc793.txt
 *
 * @param block the beginning of a block of memory with TCP pseudo header and TCP header
 * @param block_len the length of the provided block of memory
 *
 * @return TCP header checksum
 */
uint16_t tcp_checksum(uint16_t *block, int block_len) {
    uint32_t sum = 0;

    // "Sum of all 16 bit words in the header and text"
    for (; block_len > 1; block_len -= 2, block++) {
        sum += *block;
    }

    // "Od number of header and text octets"
    // "the last octet is padded on the right with zeros to forma 16 but word"
    if (block_len == 1) {
        uint16_t extra = 0;
        *(uint8_t *) (&extra) = (uint8_t) *block;
        sum += extra;
    }

    // "The checksum field is the 16 bit one's complement of the one's"
    // "complement sum of all 16 bit words in the header and text"
    sum = (sum >> 16) + (sum & 0xffff);
    sum += (sum >> 16);

    return (uint16_t) ~sum;
}

/**
 * @brief Prepares TCP SYN packet for a specific target
 * @details TCP header differentiates between the usage of IPv4 and IPv6. This
 * function is ready for this scenario and assembles the packet (or rather TCP
 * header) with this difference in mind. Sequence number is always set to 0.
 * There's no need to handle that number further as we do not plan to interact
 * with the target more.
 *
 * @param ipk_tcphdr pointer to TCP header
 * @param target scanned server
 * @param port scanned port
 * @param ipv6 use IPv6
 */
void prepare_tcp_syn_packet(struct ipk_tcphdr *ipk_tcphdr, struct addrinfo *target, int port, bool ipv6) {
    uint32_t pseudo_tcphdr_size = ipv6 ? sizeof(struct pseudo_tcphdr_ipv6) : sizeof(struct pseudo_tcphdr_ipv4);
    char tcp_header_block[pseudo_tcphdr_size + sizeof(struct ipk_tcphdr)];
    // Convenience pointer
    struct tcphdr *tcphdr = (struct tcphdr *) ipk_tcphdr;
    // IPv4 specific variables
    struct pseudo_tcphdr_ipv4 pseudo_tcphdr_ipv4;
    struct sockaddr_in *target_ipv4 = (struct sockaddr_in *) target->ai_addr;
    struct sockaddr_in *if_ipv4 = (struct sockaddr_in *) selected_ifaddr->ifa_addr;
    // IPv6 specific variables
    struct pseudo_tcphdr_ipv6 pseudo_tcphdr_ipv6;
    struct sockaddr_in6 *target_ipv6 = (struct sockaddr_in6 *) target->ai_addr;
    struct sockaddr_in6 *if_ipv6 = (struct sockaddr_in6 *) selected_ifaddr->ifa_addr;

    // Fill the header with zeroes
    memset(ipk_tcphdr, 0, sizeof(struct ipk_tcphdr));

    // TCP header
    // Values for fields Window and Maximum Segment Size were inspired by
    // values set by Nmap.
    // From hex to dec:
    // 0x0400 -> 1024
    tcphdr->source = htons(app_port);
    tcphdr->dest = htons(port);
    tcphdr->doff = sizeof(struct ipk_tcphdr) / 4;
    tcphdr->syn = 1;
    tcphdr->window = htons(0x0400);

    // TCP header -> Option
    ipk_tcphdr->kind = 2;
    ipk_tcphdr->len = 4;
    ipk_tcphdr->mss = htons(1460);

    // TCP Pseudo header; needed for calculating checksum
    // It is needed to differentiate between IPv6 and IPv4 addresses
    if (ipv6) {
        memset(&pseudo_tcphdr_ipv6, 0, pseudo_tcphdr_size);

        pseudo_tcphdr_ipv6.src_addr = (__uint128_t) *if_ipv6->sin6_addr.s6_addr;
        pseudo_tcphdr_ipv6.dst_addr = (__uint128_t) *target_ipv6->sin6_addr.s6_addr;
        pseudo_tcphdr_ipv6.ulp_len = htons(sizeof(struct ipk_tcphdr));
    
        // Populate temporary structure for computing the checksum
        memcpy(tcp_header_block, &pseudo_tcphdr_ipv6, pseudo_tcphdr_size);
        memcpy(tcp_header_block + pseudo_tcphdr_size, ipk_tcphdr, sizeof(struct ipk_tcphdr));

    } else {
        memset(&pseudo_tcphdr_ipv4, 0, pseudo_tcphdr_size);

        pseudo_tcphdr_ipv4.src_addr = (uint32_t) if_ipv4->sin_addr.s_addr;
        pseudo_tcphdr_ipv4.dst_addr = (uint32_t) target_ipv4->sin_addr.s_addr;
        pseudo_tcphdr_ipv4.protocol = IPPROTO_TCP;
        pseudo_tcphdr_ipv4.tcp_len = htons(sizeof(struct ipk_tcphdr));
    
        // Populate temporary structure for computing the checksum
        memcpy(tcp_header_block, &pseudo_tcphdr_ipv4, pseudo_tcphdr_size);
        memcpy(tcp_header_block + pseudo_tcphdr_size, ipk_tcphdr, sizeof(struct ipk_tcphdr));
    }

    tcphdr->check = tcp_checksum((uint16_t *) tcp_header_block, sizeof(tcp_header_block));
}

/**
 * @brief Scans port of a target using SYN TCP packet
 * @details Depending on the outcome of the scan the function writes to the
 * standard output. The printed text is always in the form of '<port>/tcp
 * <status>' where <status> is either 'open', 'closed', 'filtered' or 'failed'.
 * In case the scan failed, the reason is printed to standard error.
 *
 * @param sock file descriptor refering to target
 * @param target scanned server
 * @param port scanned port
 */
void scan_tcp_port(int sock, struct addrinfo *target, int port) {
    int poll_rc;
    char res[BUFSIZ];
    bool repeated = false;
    struct pollfd fds;
    struct timespec timeout;
    struct ipk_tcphdr sent_tcphdr;
    struct iphdr rec_iphdr;
    struct tcphdr rec_tcphdr;

    fds.fd = sock;
    fds.events = POLLIN;

    timeout.tv_sec = scan_timeout / 1000;
    timeout.tv_nsec = (scan_timeout % 1000) * 1000;

    prepare_tcp_syn_packet(&sent_tcphdr, target, port, target->ai_family == AF_INET6 ? true : false);

send:
    if (sendto(sock, &sent_tcphdr, sizeof(struct ipk_tcphdr), 0, target->ai_addr, target->ai_addrlen) == -1) {
        warn("failed to send TCP SYN to port %d", port);
        goto failed;
    }

    poll_rc = ppoll(&fds, 1, &timeout, NULL);
    if (poll_rc == -1) {
        warn("failed to poll events on port %d", port);
        goto failed;
    }

    // First message without a response -> resend SYN packet
    if (!(fds.revents & POLLIN) && !repeated) {
        repeated = true;
        goto send;
    }

    // Second message without a response -> filtered port
    if (!(fds.revents & POLLIN) && repeated) {
        goto filtered;
    }

    if (recvfrom(sock, res, BUFSIZ, 0, target->ai_addr, &target->ai_addrlen) == -1) {
        warn("failed to receive TCP response from port %d", port);
        goto failed;
    }

    // The response always starts with an IP header.
    rec_iphdr = *(struct iphdr *) res;
    rec_tcphdr = *(struct tcphdr *) (res + sizeof(struct iphdr));

    if (rec_iphdr.protocol != IPPROTO_TCP) {
        // Possibly an ICMP message was received. No processing is in place, so
        // it is easier to resend the SYN packet just like when there's no
        // response. The second time ending up here means the port is filtered.
        // In case this is ever handled, the related ICMP errors are:
        // - type 3, code 1, 2, 3, 9, 10, or 13)
        if (repeated) {
            goto filtered;
        }

        repeated = true;
        goto send;
    }

    if (rec_tcphdr.ack == 1 && rec_tcphdr.syn == 1) {
        printf("%d/tcp open\n", port);
    } else if (rec_tcphdr.rst == 1) {
        printf("%d/tcp closed\n", port);
    } else {
        if (repeated) {
            goto filtered;
        }

        repeated = true;
        goto send;
    }
    return;

filtered:
    printf("%d/tcp filtered\n", port);
    return;

failed:
    printf("%d/tcp failed\n", port);
    return;
}

/**
 * @brief Scans ports of a target using TCP SYN packets
 *
 * @param target scanned server
 *
 * @return success
 *
 * @retval 0 success
 * @retval EXIT_FAILURE failure
 */
int scan_tcp_ports(struct addrinfo *target) {
    struct addrinfo hints;
    int sock, rc;

    if (tcp_port_pool == NULL) {
        return 0;
    }
    
    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_RAW;
    hints.ai_protocol = IPPROTO_TCP;
    rc = getaddrinfo(target_name, NULL, &hints, &target);
    if (rc != 0) {
        warnx("failed to resolve target' address for tcp scan: %s", gai_strerror(rc));
        return EXIT_FAILURE;
    }

    sock = socket(target->ai_family, target->ai_socktype, target->ai_protocol);
    if (sock == -1) {
        warn("failed to open raw socket");
        return EXIT_FAILURE;
    }

    if (setsockopt(sock, SOL_SOCKET, SO_BINDTODEVICE, interface_name, strlen(interface_name) + 1) != 0) {
        warn("failed to bind socket to interface");
        return EXIT_FAILURE;
    }

    for (int i = 0; i < tcp_port_poolnd; i++) {
        int port, port_start, port_end;

        port_start = tcp_port_pool[i].start;
        port_end = tcp_port_pool[i].end;

        port = port_start;

        if (tcp_port_pool[i].end != -1) {
            for (; port <= port_end; port++) {
                scan_tcp_port(sock, target, port);
            }
            continue;
        }

        scan_tcp_port(sock, target, port);
    }

    return 0;
}

/**
 * @brief Scans ports of a target using UDP
 * @detail not implemented
 *
 * @param target scanned server
 *
 * @return success
 *
 * @retval 0 success
 * @retval EXIT_FAILURE failure
 */
int scan_udp_ports(struct addrinfo *target) {
    if (udp_port_pool == NULL) {
        return 0;
    }

    printf("Scanning of ports using UDP ICMP port unreachable is not implemented.\n");    

    return 0;
}

/**
 * @brief Aggregate function for scanning TCP/UDP ports
 * @details Even in case of a failure, both subscans (TCP/UDP) are executed.
 *
 * @return success
 *
 * @retval EXIT_SUCCESS success
 * @retval EXIT_FAILURE failure
 */
int scan_ports() {
    struct addrinfo *target;
    char hostname[1024];
    int tcp_scan_rc, udp_scan_rc, rc;

    rc = getaddrinfo(target_name, NULL, NULL, &target);
    if (rc != 0) {
        errx(EXIT_FAILURE, "failed to resolve target' address: %s", gai_strerror(rc));
    }

    rc = getnameinfo(target->ai_addr, target->ai_addrlen, hostname, 1024, NULL, 0, NI_NUMERICHOST);
    if (rc != 0) {
        errx(EXIT_FAILURE, "failed to get host adress: %s", gai_strerror(rc));
    }

    // Find the requested network interface
    for (selected_ifaddr = first_ifaddrs; selected_ifaddr != NULL; selected_ifaddr = selected_ifaddr->ifa_next) {
        // The protocol family has to match
        if (selected_ifaddr->ifa_addr == NULL || selected_ifaddr->ifa_addr->sa_family != target->ai_family) {
            continue;
        }
        
        if (strcmp(selected_ifaddr->ifa_name, interface_name) != 0) {
            continue;
        }

        break;
    }

    if (selected_ifaddr == NULL) {
        errx(EXIT_FAILURE, "no valid interface with name %s", interface_name);
    }

    printf("Status of ports on %s(%s):\n", target_name, hostname);

    tcp_scan_rc = scan_tcp_ports(target);
    udp_scan_rc = scan_udp_ports(target);

    freeaddrinfo(target);

    if (tcp_scan_rc == EXIT_SUCCESS && udp_scan_rc == EXIT_SUCCESS) {
        return EXIT_SUCCESS;
    } else {
        return EXIT_FAILURE;
    }
}

/**
 * @brief Prints the name and assigned IP address of active network interfaces
 */
void list_interfaces() {
    struct ifaddrs *ifaddr;
    char if_addr[1024];
    int rc;
    socklen_t ip_type_size;

    printf("Active network interfaces:\n");
    for (ifaddr = first_ifaddrs; ifaddr != NULL; ifaddr = ifaddr->ifa_next) {
        // Only list active interface// Only list active interfacess
        if (ifaddr->ifa_addr == NULL) {
            continue;
        }

        if (ifaddr->ifa_addr->sa_family == AF_INET6) {
            ip_type_size = sizeof(struct sockaddr_in6);
        } else if (ifaddr->ifa_addr->sa_family == AF_INET) {
            ip_type_size = sizeof(struct sockaddr_in);
        } else {
            // We're only interested in IPv4 or IPv6
            continue;
        }

        rc = getnameinfo(ifaddr->ifa_addr, ip_type_size, if_addr, sizeof(if_addr), NULL, 0, NI_NUMERICHOST);
        if (rc != 0) {
            warnx("failed to get address of interface %s: %s", ifaddr->ifa_name, gai_strerror(rc));
            continue;
        }

        // 16 characters wide column should cover most cases. It is to create
        // a nicely aligned output.
        printf("%-16s %s\n", ifaddr->ifa_name, if_addr);
    }
}

void parse_args(int argc, char *argv[]) {
    int option, option_index = 0;
    char *port_str = NULL, *port_start_str = NULL, *port_end_str = NULL,
         *port_str_rest = NULL, *optarg_rest = NULL, *endptr = NULL;

    while ((option = getopt_long(argc, argv, "i::t:u:w:", long_options, &option_index)) != -1) {
        switch (option) {
        case 'i':
            if (optarg == NULL
                && argv[optind] != NULL
                && argv[optind][0] != '-') {
                optarg = argv[optind++];
            }
            interface_name = optarg;

            if (interface_name == NULL) {
                continue;
            }
            
            break;
        case 't':
            // There is always at least one section
            tcp_port_poolnd = 1;

            if (tcp_port_pool != NULL) {
                errx(EXIT_FAILURE, "scanned tcp ports already set");
            }

            // Count the number of ports separated by a comma -> sections
            for (unsigned int i = 0; i < strlen(optarg); i++) {
                if (optarg[i] == ',') {
                    tcp_port_poolnd++;
                }
            }

            tcp_port_pool = malloc(tcp_port_poolnd * sizeof(struct port_range));
            if (tcp_port_pool == NULL) {
                err(EXIT_FAILURE, "failed to alloc tcp port pool");
            }

            // For each section differentiate between a range and a single port
            port_str = strtok_r(optarg, ",", &optarg_rest);
            for (int i = 0; i < tcp_port_poolnd; i++) {
                if (port_str == NULL) {
                    errx(EXIT_FAILURE, "invalid port value");
                }
                tcp_port_pool[i].end = -1;
    
                port_start_str = strtok_r(port_str, "-", &port_str_rest);
                port_end_str = strtok_r(NULL, "-", &port_str_rest);

                errno = 0;
                tcp_port_pool[i].start = strtol(port_start_str, &endptr, 10);
                if (*endptr != '\0' || errno != 0) {
                    errx(EXIT_FAILURE, "invalid port value %s", port_start_str);
                }
                
                if (port_end_str != NULL) {
                    errno = 0;
                    tcp_port_pool[i].end = strtol(port_end_str, &endptr, 10);
                    if (*endptr != '\0' || errno != 0) {
                        errx(EXIT_FAILURE, "invalid port value %s", port_end_str);
                    }

                    if (tcp_port_pool[i].start > tcp_port_pool[i].end) {
                        errx(EXIT_FAILURE, "starting port can't be greater than ending port");
                    }
                }

                port_str = strtok_r(NULL, ",", &optarg_rest);
            }
            break;
        case 'u':
            // There is always at least one section
            udp_port_poolnd = 1;

            if (udp_port_pool != NULL) {
                errx(EXIT_FAILURE, "scanned udp ports already set");
            }

            // Count the number of ports separated by a comma -> sections
            for (unsigned int i = 0; i < strlen(optarg); i++) {
                if (optarg[i] == ',') {
                    udp_port_poolnd++;
                }
            }

            udp_port_pool = malloc(udp_port_poolnd * sizeof(struct port_range));
            if (udp_port_pool == NULL) {
                err(EXIT_FAILURE, "failed to alloc tcp port pool");
            }

            // For each section differentiate between a range and a single port
            port_str = strtok_r(optarg, ",", &optarg_rest);
            for (int i = 0; i < udp_port_poolnd; i++) {
                if (port_str == NULL) {
                    errx(EXIT_FAILURE, "invalid port value");
                }
                udp_port_pool[i].end = -1;
    
                port_start_str = strtok_r(port_str, "-", &port_str_rest);
                port_end_str = strtok_r(NULL, "-", &port_str_rest);

                errno = 0;
                udp_port_pool[i].start = strtol(port_start_str, &endptr, 10);
                if (*endptr != '\0' || errno != 0) {
                    errx(EXIT_FAILURE, "invalid port value %s", port_start_str);
                }
                
                if (port_end_str != NULL) {
                    errno = 0;
                    udp_port_pool[i].end = strtol(port_end_str, &endptr, 10);
                    if (*endptr != '\0' || errno != 0) {
                        errx(EXIT_FAILURE, "invalid port value %s", port_end_str);
                    }

                    if (udp_port_pool[i].start > udp_port_pool[i].end) {
                        errx(EXIT_FAILURE, "starting port can't be greater than ending port");
                    }
                }

                port_str = strtok_r(NULL, ",", &optarg_rest);
            }
            break;
        case 'w':
            errno = 0;
            scan_timeout = strtol(optarg, &endptr, 10);
            if (*endptr != '\0' || errno != 0) {
                errx(EXIT_FAILURE, "invalid timeout value %s", optarg);
            }
            break;
        default:
            exit(EXIT_FAILURE);
            break;
        }
    }

    if (optind >= argc) {
        errx(EXIT_FAILURE, "expected 1 argument");
    }

    if (tcp_port_pool == NULL && udp_port_pool == NULL) {
        errx(EXIT_FAILURE, "no port to be scanned");
    }

    target_name = argv[optind];
}

/**
 * @brief Destructor of global variables
 */
void finalize() {
    if (tcp_port_pool != NULL) {
        free(tcp_port_pool);
    }

    if (udp_port_pool != NULL) {
        free(udp_port_pool);
    }

    if (first_ifaddrs != NULL) {
        freeifaddrs(first_ifaddrs);
    }
}

int main(int argc, char *argv[]) {
    atexit(finalize);

    // Prepare list of interfaces; needed for binding socket and packet assembly
    if (getifaddrs(&first_ifaddrs) != 0) {
        err(EXIT_FAILURE, "failed to get local network interfaces");
    }

    parse_args(argc, argv);

    if (interface_name == NULL) {
        list_interfaces();
        return EXIT_SUCCESS;
    }

    if (scan_ports() != 0) {
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
