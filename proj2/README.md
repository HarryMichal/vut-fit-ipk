# IPK 2021 - Project 2 - Port scanner

Solution for Project 2 of course IPK - Port scanner (variant OMEGA).

## Overview

Supports:

- all commandline options and arguments
- interface listing
- scanning using TCP SYN packets

Does not support:

- scanning using UDP ICMP port unreachable

## Build from source

Prerequisites:

- C compiler (gnu17 compliant)
- make

```
$ make
```

## Usage

```shell
# Scan of ports 80 and 300-320 with default timeout (5000ms)
$ ipk-l4-scan -p 80,300-320 -i wlan0 <domain/ip address>

# Scan of ports 80 with custom timeout
$ ipk-l4-scan -p 80 -i wlan0 -w 1200 <domain/ip address>

# Listing of all active network interfaces (ports must be set)
$ ipk-l4-scan -p 80
```

## List of files

- ipk-l4-scan.c
- Makefile
- manual.pdf
- README.md
- COPYING
