# Computer Communications and Networks 2021

- spring semester
- An insightful course about the fundamentals of networking. Well presented but needs updating implementation details.

## Projects

### Client for trivial distributed filesystem

File access used File Service Protocol (FSP) and IP address translation used Name Service Protocol (NSP). For FSP we had to implement only the `GET` request.

### Scanner of network services (Port scanner)

TCP and UDP scanner of network services. TCP scanning involves sending of SYN packets without completing the 3-way-handshake. UDP scanning involved expecting an ICMP response of type 3 (port unreachable). The project had to be coupled with a documentation.
